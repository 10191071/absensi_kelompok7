package com.rezayusrilnaufal_10191071.finalproject_kelompok7;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class Profile extends AppCompatActivity {

    Button btn_back, btn_logout, btn_ubah;
    CircleImageView iv_foto;
    EditText et_nama, et_nip, et_alamat, et_kontak;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        btn_back = findViewById(R.id.btn_back);
        btn_logout = findViewById(R.id.btn_logout);
        btn_ubah = findViewById(R.id.btn_ubah);
        iv_foto = findViewById(R.id.foto);
        et_nama = findViewById(R.id.nama);
        et_nip = findViewById(R.id.nip);
        et_alamat = findViewById(R.id.alamat);
        et_kontak = findViewById(R.id.kontak);

        sharedPreferences = getSharedPreferences("apreswa", MODE_PRIVATE);

        getData();

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btn_ubah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ubahData();
            }
        });

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.remove("user_id");
                editor.apply();

                Intent intent = new Intent(Profile.this, Login.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void ubahData() {
        String url = getString(R.string.url_api) + "ubah";
        JSONObject data = new JSONObject();
        try {
            data.put("user_id", sharedPreferences.getString("user_id", null));
            data.put("alamat", et_alamat.getText().toString());
            data.put("no_telp", et_kontak.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, data,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Toast.makeText(Profile.this, response.getString("keterangan"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Profile.this, "Gagal menghubungkan", Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue rq = Volley.newRequestQueue(this);
        rq.add(request);
    }

    private void getData() {
        String url = getString(R.string.url_api) + "get-user/" + sharedPreferences.getString("user_id", null);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            et_nama.setText(response.getString("nama"));
                            et_nip.setText(response.getString("nip"));
                            et_alamat.setText(response.getString("alamat"));
                            et_kontak.setText(response.getString("no_telp"));
                            Glide.with(Profile.this).load(getString(R.string.url_foto) + response.getString("foto")).into(iv_foto);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Profile.this, "Gagal menghubungkan", Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue rq = Volley.newRequestQueue(this);
        rq.add(request);
    }
}
package com.rezayusrilnaufal_10191071.finalproject_kelompok7;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    Button btn_profil, btn_cekin, btn_cekout;
    String jam;
    TextView tv_checkin, tv_checkout;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_profil = findViewById(R.id.btn_profil);
        btn_cekin = findViewById(R.id.checkinbtn);
        btn_cekout = findViewById(R.id.checkoutbtn);
        tv_checkin = findViewById(R.id.main_cekin);
        tv_checkout = findViewById(R.id.main_cekout);

        sharedPreferences = getSharedPreferences("apreswa", MODE_PRIVATE);

        cekWaktu();
        getCekinCekout();

        btn_profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Profile.class);
                startActivity(intent);
            }
        });
    }

    private void getCekinCekout() {
        String url = getString(R.string.url_api) + "get-cekin-cekout/" + sharedPreferences.getString("user_id", null);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getString("cekin").equals("null")) {
                                tv_checkin.setText("Belum");
                            } else {
                                tv_checkin.setText(response.getString("cekin"));
                            }
                            if(response.getString("cekout").equals("null")) {
                                tv_checkout.setText("Belum");
                            } else {
                                tv_checkout.setText(response.getString("cekout"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "Gagal menghubungkan", Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(request);
    }

    private void cekWaktu() {
        DateFormat df = new SimpleDateFormat("HH", Locale.getDefault());
        jam = df.format(new Date());
        
        if(Integer.parseInt(jam) >= 7 && Integer.parseInt(jam) < 8) {
            btn_cekin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    IntentIntegrator intentIntegrator = new IntentIntegrator(MainActivity.this);
                    intentIntegrator.setPrompt("For flash use volume key up");
                    intentIntegrator.setBeepEnabled(true);
                    intentIntegrator.setOrientationLocked(true);
                    intentIntegrator.setCaptureActivity(Capture.class);

                    intentIntegrator.initiateScan();
                }
            });
            btn_cekout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(MainActivity.this, "Belum saatnya untuk Check Out", Toast.LENGTH_SHORT).show();
                }
            });
        } else if(Integer.parseInt(jam) >= 16 && Integer.parseInt(jam) < 22) {
            btn_cekout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    IntentIntegrator intentIntegrator = new IntentIntegrator(MainActivity.this);
                    intentIntegrator.setPrompt("For flash use volume key up");
                    intentIntegrator.setBeepEnabled(true);
                    intentIntegrator.setOrientationLocked(true);
                    intentIntegrator.setCaptureActivity(Capture.class);

                    intentIntegrator.initiateScan();
                }
            });
            btn_cekin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(MainActivity.this, "Belum saatnya untuk Check In", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else {
            btn_cekin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(MainActivity.this, "Belum saatnya untuk Check In", Toast.LENGTH_SHORT).show();
                }
            });
            btn_cekout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(MainActivity.this, "Belum saatnya untuk Check Out", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        IntentResult intentResult = IntentIntegrator.parseActivityResult(
                requestCode, resultCode, data
        );

        if (intentResult.getContents() != null) {

            JSONObject datajson = new JSONObject();
            try {
                datajson.put("user_id", sharedPreferences.getString("user_id", null));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, intentResult.getContents(), datajson,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Toast.makeText(MainActivity.this, response.getString("keterangan"), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            overridePendingTransition(0, 0);
                            finish();
                            overridePendingTransition(0, 0);
                            startActivity(getIntent());
                            overridePendingTransition(0, 0);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(MainActivity.this, "Gagal menghubungkan", Toast.LENGTH_SHORT).show();
                }
            });
            RequestQueue rq = Volley.newRequestQueue(this);
            rq.add(request);

        } else {}
    }
}